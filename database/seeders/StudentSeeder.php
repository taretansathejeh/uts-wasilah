<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            'nama' => 'Renaldi Ahmad',
            'jeniskelamin' => 'putra',
            'notelpon' => '089333456456',
            'alamat' => 'Desa Sukamuljo',
            'nomerinduksantri' => '997972'
        ]);
    }
}
