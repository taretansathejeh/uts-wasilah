<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index(){
        $data = Student::all();
        return view('datasantri',compact('data'));
    }

    public function tambahsantri(){
        return view('tambahdata');
    }

    public function insertdata(Request $request){

        Student::create($request->all());
        return redirect()->route('santri');

    }

    public function tampilkandata($id)
    {
        $data = Student::find($id);
        // dd($data);
        return view('tampildata',compact('data'));
    }

    public function updatedata(Request $request, $id)
    {
        $data = Student::find($id);
        $data->update($request->all());
        return redirect()->route('santri');
    }

    public function tentang($id)
    {
        $tentang = Student::find($id);
        return view('tentang',compact('tentang'));
    }

    public function delete($id)
    {
        $data = Student::find($id);
        $data->delete();
        return redirect()->route('santri');
    }
}


