<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/santri',[StudentController::class, 'index'])->name('santri');
Route::get('/tambahsantri',[StudentController::class, 'tambahsantri'])->name('tambahsantri');
Route::post('/insertdata',[StudentController::class, 'insertdata'])->name('insertdata');
Route::get('/tampilkandata/{id}',[StudentController::class, 'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}',[StudentController::class, 'updatedata'])->name('updatedata');
Route::get('/delete/{id}',[StudentController::class, 'delete'])->name('delete');
Route::get('/tentang/{id}',[StudentController::class, 'tentang'])->name('tentang');
